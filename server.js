const {Application, Router, FrontController} = require('./lib'),
      config                                 = require('./config'),
      application                            = new Application(config),
      router                                 = new Router();

FrontController.configure(config.controller);

application.setRouter(router);
application.setFrontController(FrontController);
application.start();
