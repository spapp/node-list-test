const Path = require('path');

module.exports = {
    port:            3000,
    routes:          [
        {
            controller: 'user',
            path:       '/user',
            method:     'get'
        }
    ],
    controller:      {
        path:          Path.join(__dirname, 'lib', 'domain', '{domain}', 'controller'),
        handlerSuffix: 'Handler'
    }
};
