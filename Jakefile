const FileSystem      = require('fs'),
      Path            = require('path'),
      Util            = require('util'),
      ENV_PRODUCTION  = 'production',
      ENV_DEVELOPMENT = 'development',
      PROJECT_PATH    = process.env.PWD,
      PROCESS_ENV     = process.env.ENVIRONMENT || process.env.environment || process.env.ENV || process.env.env,
      ENVIRONMENT     = [ENV_PRODUCTION, 'prod'].indexOf(PROCESS_ENV) > -1 ? ENV_PRODUCTION : ENV_DEVELOPMENT,
      DB_DIR          = Path.join(PROJECT_PATH, 'database'),
      DB_FILENAME     = 'db.json',
      DB_FILE         = Path.join(DB_DIR, DB_FILENAME),
      firstNames      = ['János', 'Géza', 'Ubul', 'Kata', 'Rita', 'Iza', 'Peti', 'Irén'],
      lastNames       = ['Nagy', 'Kiss', 'Réz', 'Oláh', 'Kovács', 'Kis', 'Papp', 'Pap', 'Pa'],
      domains         = ['ssas', 'jdfjhfds', 'dhdsjh', 'oidsod', 'kjasdhksdj', 'asdjkasdjh'],
      tlds            = ['hu', 'com', 'org'];


namespace('clean', function () {
    desc('Clean test database');
    task('db', function () {
        FileSystem.unlinkSync(DB_FILE);
    });

});

namespace('db', function () {

    desc('Create test database');
    task('create', function () {
        FileSystem.writeFileSync(DB_FILE, '[]');
    });

    desc('Add handred test user');
    task('add', function () {
        let users = JSON.parse(FileSystem.readFileSync(DB_FILE));

        for (let i = 0; i < 100; i++) {
            let firstName = genFirstName(),
                lastName  = genLastName();

            users.push({
                id:         users.length + 1,
                first_name: firstName,
                last_name:  lastName,
                email:      genEmail(firstName, lastName)
            });
        }

        FileSystem.writeFileSync(DB_FILE, JSON.stringify(users));
    });

});

/**
 * Helper method
 *
 * Generates a random first name
 *
 * @returns {string}
 */
function genFirstName() {
    let x = randomInt(0, firstNames.length - 1);

    return firstNames[x];
}

/**
 * Helper method
 *
 * Generates a random last name
 *
 * @returns {string}
 */
function genLastName() {
    let x = randomInt(0, lastNames.length - 1);

    return lastNames[x];
}

/**
 * Helper method
 *
 * Generates a random dummy email adress
 *
 * @returns {string}
 */
function genEmail(firstName, lastName) {
    let domain = domains[randomInt(0, domains.length - 1)],
        tld    = tlds[randomInt(0, tlds.length - 1)];

    return Util.format('%s.%s@%s.%s', firstName, lastName, domain, tld).toLowerCase();
}

/**
 * Generates a random integer number
 *
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
