#!/usr/bin/env bash
#

PROJECT_DIR="/vagrant"

export DEBIAN_FRONTEND=noninteractive

apt-get update -y
apt-get install -yq nodejs npm

# bash
cat <<EOF > /home/vagrant/.bashrc

PS1="\e[30;46m \h \e[36;43m \e[30;43m\w \e[0m\e[33;40m \e[0;0m"
EOF

# nodejs
echo "export PATH=${PROJECT_DIR}/bin:${PATH}" >> /home/vagrant/.bashrc

npm install -g jake mocha apidoc
