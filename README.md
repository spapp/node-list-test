# HTTP API test

This is a possible solution how to implement an HTTP API without any framework.
Keep in your mind: this is a test. Not a complet solution.

## Getting Started

### `Prerequisites`
- Vagrant
- Virtual Box
- NODE.js v8.10.0

### `Installing`

#### `Installing dev environment`

1. git clone https://bitbucket.org/spapp/node-list-test.git
2. cd /path/to/node-list-test
3. vagrant up
4. vagrant ssh
5. cd /vagrant

#### `Create test database`

Run `jake -ls` command in the `/vagrant` folder.
You can see all task.

Run all of the following commands:
```
jake db:create
jake db:add
```

If you would like to add hundred test data again then run `jake db:add` command again, again...

## Running the tests

Run `npm test` command in the `/vagrant` folder.

## Execute the server

Run `npm start` command in the `/vagrant` folder.

## Generate documentation

Run `npm run doc` command in the `/vagrant` folder.

After open the `doc/index.html` file in a browser.

## License

**MIT License**

Copyright 2020 spappsite.hu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.