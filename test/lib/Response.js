const Assert   = require('assert'),
      Response = require('../../lib/Response');

describe('Response', function () {
    it('should be exist', function () {
        Assert.strictEqual(typeof Response, 'function');
    });

    describe('.CONTENT_TYPE_JSON', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.CONTENT_TYPE_JSON, 'string');
        });

        it('should be "application/json"', function () {
            Assert.strictEqual(Response.CONTENT_TYPE_JSON, 'application/json');
        });
    });

    describe('.CONTENT_TYPE_TEXT', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.CONTENT_TYPE_TEXT, 'string');
        });

        it('should be "text/plain"', function () {
            Assert.strictEqual(Response.CONTENT_TYPE_TEXT, 'text/plain');
        });
    });

    describe('.HEADER_CONTENT_TYPE', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.HEADER_CONTENT_TYPE, 'string');
        });

        it('should be "Content-Type"', function () {
            Assert.strictEqual(Response.HEADER_CONTENT_TYPE, 'Content-Type');
        });
    });


    describe('#setData()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.prototype.setData, 'function');
        });
    });

    describe('#setStatusCode()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.prototype.setStatusCode, 'function');
        });
    });

    describe('#getStatusCode()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.prototype.getStatusCode, 'function');
        });

        it('should be 200 by default', function () {
            Assert.strictEqual((new Response()).getStatusCode(), 200);
        });

        it('should be 400 if the data is an error', function () {
            Assert.strictEqual((new Response(new Error())).getStatusCode(), 400);
        });

        it('should be setted value', function () {
            let response = new Response();

            response.setStatusCode(201);
            Assert.strictEqual(response.getStatusCode(), 201);

            response.setStatusCode(301);
            Assert.strictEqual(response.getStatusCode(), 301);
        });
    });

    describe('#getContentType()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.prototype.getContentType, 'function');
        });

        it('should be "' + Response.CONTENT_TYPE_TEXT + '" if the data is an error', function () {
            Assert.strictEqual((new Response(new Error())).getContentType(), Response.CONTENT_TYPE_TEXT);
        });

        it('should be "' + Response.CONTENT_TYPE_TEXT + '" if the data is an text', function () {
            Assert.strictEqual((new Response('test')).getContentType(), Response.CONTENT_TYPE_TEXT);
        });

        it('should be "' + Response.CONTENT_TYPE_JSON + '" if the data is an object', function () {
            Assert.strictEqual((new Response({})).getContentType(), Response.CONTENT_TYPE_JSON);
        });
    });

    describe('#getHeaders()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.prototype.getHeaders, 'function');
        });
    });

    describe('#toString()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Response.prototype.toString, 'function');
        });

        it('should return error message', function () {
            let message = 'Test Error';

            Assert.strictEqual((new Response(new Error(message))).toString(), message);
        });

        it('should return JSON text', function () {
            let json = {foo: 'foo', bar: 12};

            Assert.strictEqual((new Response(json)).toString(), JSON.stringify(json));
        });
    });
});
