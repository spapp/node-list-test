const Assert      = require('assert'),
      Application = require('../../lib/Application');

describe('Application', function () {
    it('should be exist', function () {
        Assert.strictEqual(typeof Application, 'function');
    });

    describe('#setRouter()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Application.prototype.setRouter, 'function');
        });
    });

    describe('#getRouter()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Application.prototype.getRouter, 'function');
        });
    });

    describe('#setFrontController()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Application.prototype.setFrontController, 'function');
        });
    });

    describe('#getFrontController()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Application.prototype.getFrontController, 'function');
        });
    });

    describe('#start()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Application.prototype.start, 'function');
        });
    });
});
