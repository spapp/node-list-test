const Assert      = require('assert'),
      Querystring = require('querystring'),
      Router      = require('../../lib/Router');

describe('Router', function () {
    it('should be exist', function () {
        Assert.strictEqual(typeof Router, 'function');
    });

    describe('#addRoute()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof Router.prototype.addRoute, 'function');
        });
    });

    describe('#route()', function () {
        let pathA        = '/path',
            pathB        = '/path/test',
            controller   = 'test',
            methodGet    = 'get',
            methodDelete = 'delete',
            params       = {foo: 'foo', bar: 12},
            requestA     = {method: methodGet, url: pathA + '?' + Querystring.stringify(params)},
            requestB     = {method: methodDelete, url: pathA + '?' + Querystring.stringify(params)},
            requestC     = {method: methodGet, url: pathB + '?' + Querystring.stringify(params)},
            router;

        before(function () {
            router = new Router();

            router.addRoute(controller, pathA, methodGet);
        });

        it('should be exist', function () {
            Assert.strictEqual(typeof Router.prototype.route, 'function');
        });

        it('should be route', function () {
            Assert.deepEqual(router.route(requestA), {
                path:       pathA,
                method:     methodGet.toUpperCase(),
                controller: controller,
                params:     params
            });
        });

        it('should not be route', function () {
            Assert.strictEqual(typeof router.route(requestB), 'undefined');
            Assert.strictEqual(typeof router.route(requestC), 'undefined');
        });
    });
});

/*
let routes   = this.routes,
            method   = request.method,
            urlParts = request.url.split('?'),
            path     = urlParts[0] || '/',
            params   = Querystring.parse(urlParts[1] || ''),
            route;

        if (routes[path] && method.toUpperCase() === routes[path].method.toUpperCase()) {
            route = {
                path:       path,
                method:     method.toUpperCase(),
                controller: routes[path].controller,
                params:     params
            };
        }

        return route;
 */
