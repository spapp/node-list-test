const Assert          = require('assert'),
      FrontController = require('../../lib/FrontController');

describe('FrontController', function () {
    it('should be exist', function () {
        Assert.strictEqual(Object.prototype.toString.call(FrontController), '[object Object]');
    });

    describe('.configure()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof FrontController.configure, 'function');
        });
    });

    describe('.handle()', function () {
        it('should be exist', function () {
            Assert.strictEqual(typeof FrontController.handle, 'function');
        });
    });
});
