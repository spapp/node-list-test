const Util               = require('util'),
      Response           = require('./Response'),
      PLACEHOLDER_DOMAIN = '{domain}',
      config             = {};
/**
 * @singleton
 * @type {FrontController}
 */
module.exports = {
    /**
     * Configires front controller
     * @param {Object} controllerConfig
     */
    configure: function (controllerConfig) {
        Object.assign(config, controllerConfig);
    },

    /**
     * Handle a routed incoming request
     * @param {Router~RouteInfo} routeInfo
     * @param {IncomingMessage} request
     * @param {ServerResponse} response
     */
    handle: function (routeInfo, request, response) {
        console.log(getFormattedDate(), '->', request.method.toUpperCase(), request.url);
        handleRequest(routeInfo, request, response).then((values) => flushRequest.apply(this, values));
    }
};

/**
 * Helper method
 *
 * Finish and answer a request
 *
 * @private
 * @param {Response} controllerResponse
 * @param {IncomingMessage} request
 * @param {ServerResponse} response
 */
function flushRequest(controllerResponse, request, response) {
    let responseBody = controllerResponse.toString();

    response.writeHead(controllerResponse.getStatusCode(), controllerResponse.getHeaders());
    response.write(responseBody);
    response.end();

    console.log(getFormattedDate(), '<-', response.statusCode, request.method.toUpperCase(), request.url, responseBody);
}

/**
 * Helper method
 *
 * Handles a request
 *
 * @private
 * @param {Router~RouteInfo} routeInfo
 * @param {IncomingMessage} request
 * @param {ServerResponse} response
 * @returns {Promise<*[]>}
 */
async function handleRequest(routeInfo, request, response) {
    let controllerPath     = config.path.replace(PLACEHOLDER_DOMAIN, routeInfo.controller),
        handler            = routeInfo.method.toLowerCase() + config.handlerSuffix,
        controller         = require(controllerPath),
        controllerResponse = {};

    try {
        controllerResponse = controller[handler].call(controller, routeInfo.params);
    } catch (error) {
        controllerResponse = new Response(error);
    }

    return [controllerResponse, request, response];
}

/**
 * Helper method
 *
 * Returns a formated date string
 *
 * @private
 * @returns {string}
 */
function getFormattedDate() {
    let date    = new Date(),
        dateTpl = '%s-%s-%s %s:%s:%s.%s',
        M       = String(date.getMonth() + 1).padStart(2, '0'),
        d       = String(date.getDate()).padStart(2, '0'),
        h       = String(date.getHours()).padStart(2, '0'),
        m       = String(date.getMinutes()).padStart(2, '0'),
        s       = String(date.getSeconds()).padStart(2, '0'),
        ms      = String(date.getMilliseconds()).padStart(3, '0');

    return Util.format(dateTpl, date.getFullYear(), M, d, h, m, s, ms);
}
