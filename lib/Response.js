const CONTENT_TYPE_JSON   = 'application/json',
      CONTENT_TYPE_TEXT   = 'text/plain',
      HEADER_CONTENT_TYPE = 'Content-Type';

/**
 * Response class
 * @type {Response}
 */
module.exports = class Response {
    /**
     * @constant
     */
    static get CONTENT_TYPE_JSON() {
        return CONTENT_TYPE_JSON;
    }

    /**
     * @constant
     */
    static get CONTENT_TYPE_TEXT() {
        return CONTENT_TYPE_TEXT;
    }

    /**
     * @constant
     */
    static get HEADER_CONTENT_TYPE() {
        return HEADER_CONTENT_TYPE;
    }

    /**
     * @constructor
     * @param {*} data
     */
    constructor(data) {
        this.setData(data || '');
    }

    /**
     * Set respnse data
     * @param {*} data
     */
    setData(data) {
        this.data = data;
    }

    /**
     * Returns response status code
     * @returns {number}
     */
    getStatusCode() {
        let status = this.status || 200;

        if (this.data instanceof Error) {
            status = 400;
        }

        return status;
    }

    /**
     * Sets response status code
     * @param {number} status
     */
    setStatusCode(status) {
        this.status = status;
    }

    /**
     * Returns response content type
     * @returns {string}
     */
    getContentType() {
        let contentType = CONTENT_TYPE_TEXT;

        if ('[object Object]' === Object.prototype.toString.call(this.data) || Array.isArray(this.data)) {
            contentType = CONTENT_TYPE_JSON;
        }

        return contentType;
    }

    /**
     * Returns response headers
     * @returns {{'[HEADER_CONTENT_TYPE]': string}}
     */
    getHeaders() {
        return {
            [HEADER_CONTENT_TYPE]: this.getContentType()
        };
    }

    /**
     * Returns response as a string
     * @returns {string}
     */
    toString() {
        let contentType = this.getContentType(),
            data        = this.data,
            text        = '';

        if (CONTENT_TYPE_JSON === contentType) {
            text = JSON.stringify(data);
        } else if (data instanceof Error) {
            text = data.data ? data.data.join('\n') : data.message;
        } else {
            text = '' + data;
        }

        return text;
    }
};
