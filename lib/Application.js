var Http = require('http');

/**
 * @class
 * @type {Application}
 */
module.exports = class Application {
    /**
     * @constructor
     * @param {Object} config
     */
    constructor(config) {
        this.config = config;
    }

    /**
     * Set up application router
     *
     * @param {Router} router
     * @returns {Application}
     */
    setRouter(router) {
        let routes = this.config.routes || [];

        this.router = router;

        for (let i = 0; i < routes.length; i++) {
            router.addRoute(routes[i].controller, routes[i].path, routes[i].method);
        }

        return this;
    }

    /**
     * Returns application router
     *
     * @returns {Router}
     */
    getRouter() {
        return this.router;
    }

    /**
     * Set up application front controller
     *
     * @param {FrontController} controller
     * @returns {Application}
     */
    setFrontController(controller) {
        this.frontController = controller;

        return this;
    }

    /**
     * Returns application front controller
     *
     * @returns {FrontController}
     */
    getFrontController() {
        return this.frontController;
    }

    /**
     * Starts the server
     */
    start() {
        Http.createServer((request, response) => {
            this.getFrontController().handle(
                this.getRouter().route(request),
                request,
                response
            );
        }).listen(this.config.port);
    }
};
