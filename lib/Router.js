const Querystring = require('querystring');

/**
 * Represents a route information.
 * @typedef {Object} Router~RouteInfo
 * @property {String} path - HTTP URL path.
 * @property {String} controller - Controller class name.
 * @property {String} method - HTTP method.
 * @property {Object} params - HTTP query object.
 */

/**
 * Router class
 * @type {Router}
 */
module.exports = class Router {
    /**
     * @constructor
     */
    constructor() {
        this.routes = {};
    }

    /**
     * Add a resource route
     *
     * @param {string} controller Controller class name
     * @param {string} path HTTP URL path pattern
     * @param {string} method HTTP method
     */
    addRoute(controller, path, method) {
        this.routes[path] = {controller, path, method};
    }

    /**
     * Try to route a incoming http request
     *
     * @param {IncomingMessage} request
     * @returns {RouteInfo}
     */
    route(request) {
        let routes   = this.routes,
            method   = request.method,
            urlParts = request.url.split('?'),
            path     = urlParts[0] || '/',
            params   = Querystring.parse(urlParts[1] || ''),
            route;

        if (routes[path] && method.toUpperCase() === routes[path].method.toUpperCase()) {
            route = {
                path:       path,
                method:     method.toUpperCase(),
                controller: routes[path].controller,
                params:     params
            };
        }

        return route;
    }
};
