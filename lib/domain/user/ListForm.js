const ORDER_ASC      = 'asc',
      ORDER_DESC     = 'desc',
      DEFAULT_OFFSET = 0,
      DEFAULT_LIMIT  = 20,
      DEFAULT_ORDER  = ORDER_ASC;

/**
 * Represents ListForm parameters.
 * @typedef {Object} ListForm~ListFormParams
 * @property {String} [filterField=undefined] Filter field name.
 * @property {String} [filter=undefined] Filter text.
 * @property {String} [orderField] Sort field name.
 * @property {String} [order=asc] Sort direction (asc|desc).
 * @property {Number} [limit=20] Page size.
 * @property {Number} [offset=0] Page offset.
 */

/**
 * Represents ListForm options.
 * @typedef {Object} ListForm~ListFormOptions
 * @property {String[]} fields User fields name.
 */

/**
 * @class
 * @type {ListForm}
 */
module.exports = class ListForm {
    /**
     * @constructor
     * @param {ListFormParams} params
     * @param {ListFormOptions} [options]
     */
    constructor(params, options) {
        this.options     = options || {};
        this.filterField = params.filterField;
        this.filter      = params.filter;
        this.orderField  = params.orderField;
        this.order       = (params.order || DEFAULT_ORDER).toLowerCase();
        this.limit       = parseInt(params.limit, 10) || DEFAULT_LIMIT;
        this.offset      = parseInt(params.offset, 10) || DEFAULT_OFFSET;
    }

    /**
     * Returns a form field value
     *
     * @param {string} name
     * @returns {*}
     */
    get(name) {
        return this[name];
    }

    /**
     * Validates the form values
     *
     * @returns {boolean}
     */
    validate() {
        let errors      = [],
            options     = this.options,
            filterField = this.filterField,
            orderField  = this.orderField,
            order       = this.order,
            limit       = this.limit,
            offset      = this.offset,
            filter      = this.filter;

        if ('undefined' !== typeof filterField) {
            if (options.fields.indexOf(filterField) < 0) {
                errors.push('Invalid "filterField" value');
            }

            if ('string' !== typeof filter || filter.length < 2) {
                errors.push('Invalid "filter" value (minimum 3 charachter)');
            }
        }

        if ('undefined' !== typeof orderField) {
            if (options.fields.indexOf(orderField) < 0) {
                errors.push('Invalid "orderField" value');
            }

            if ([ORDER_ASC, ORDER_DESC].indexOf(order) < 0) {
                errors.push('Invalid "order" value');
            }
        }

        if ('number' !== typeof offset || isNaN(offset) || offset < 0) {
            errors.push('Invalid "offset" value (minimum 0)');
        }

        if ('number' !== typeof limit || isNaN(limit) || limit < 5 || limit > 50) {
            errors.push('Invalid "limit" value (minimum 5 and maximum 50) ' + limit);
        }

        if (errors.length > 0) {
            let error  = new Error('Invalid query data');
            error.data = errors;

            throw error;
        }

        return true;
    }
};
