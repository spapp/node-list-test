const Repository         = require('../repository/JsonFile'), // todo need to choice repository type dynamically
      ListForm           = require('../ListForm'),
      Response           = require('../../../Response'),
      userListValidation = {
          fields: ['id', 'first_name', 'last_name', 'email']
      };

module.exports = {
    /**
     * @api {get} /user Request Users information
     * @apiName GetUser
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {String} [filterField=undefined] Filter field name.
     * @apiParam {String} [filter=undefined] Filter text.
     * @apiParam {String} [orderField] Sort field name.
     * @apiParam {String} [order=asc] Sort direction (asc|desc).
     * @apiParam {Number} [limit=20] Page size.
     * @apiParam {Number} [offset=0] Page offset.
     *
     * @apiSuccess {Number} count Count of all data.
     * @apiSuccess {Number} offset Paging offset.
     * @apiSuccess {Number} limit Page size.
     * @apiSuccess {Object[]} data Users.
     * @apiSuccess {Number} data.id User ID.
     * @apiSuccess {String} data.first_name User first name.
     * @apiSuccess {String} data.last_name User last name.
     * @apiSuccess {String} data.email User email address.
     *
     * @apiSampleRequest http://localhost:3000/user
     *
     * @apiSuccessExample Success Response
     *   HTTP/1.1 200 OK
     *   Content-Type: application/json; charset=utf-8
     *
     *   {
     *       "count": 142,
     *       "offset": 0,
     *       "limit": 5,
     *       "data": [
     *           {
     *               "id": 7,
     *               "first_name": "Géza",
     *               "last_name": "Pap",
     *               "email": "géza.pap@asdjkasdjh.com"
     *           },
     *               [...]
     *       ]
     *   }
     *
     * @apiErrorExample {json} Error-Response:
     *   HTTP/1.1 400 Bad Request
     *   Content-Type: text/plain; charset=utf-8
     *
     *   Invalid "filterField" value
     *
     * @param {object} params
     * @returns {Response}
     */
    getHandler: function (params) {
        let form = new ListForm(params, userListValidation);

        form.validate();

        return new Response(Repository.find(form));
    }
};
