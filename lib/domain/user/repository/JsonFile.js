const db = require('../../../../database/db.json'); // todo should be injected

/**
 * User information.
 * @typedef {Object} User
 * @property {Number} id - User ID.
 * @property {String} first_name - User first name.
 * @property {String} last_name - User last name.
 * @property {String} email - User email address.
 */

/**
 * User paged list response.
 * @typedef {Object} UserPagedList
 * @property {Number} count - Count of all data.
 * @property {Number} offset - Page offset.
 * @property {Number} limit - Page size.
 * @property {User[]} data - Users data.
 */
/**
 * Json file repository.
 * @typedef {Object} JsonFile
 */

/**
 * @singleton
 * @class
 * @type {JsonFile}
 */
module.exports = {
    /**
     * Find users
     * @param {ListForm} form
     * @returns {UserPagedList}
     */
    find: function (form) {
        let filterField = form.get('filterField'),
            orderField  = form.get('orderField'),
            order       = form.get('order'),
            limit       = form.get('limit'),
            offset      = form.get('offset'),
            filter      = form.get('filter'),
            response    = [];

        if (filterField && filter) {
            filterRegexp = createFilterRegexp(filter);

            for (let i = 0; i < db.length; i++) {
                if (filterRegexp.test(db[i][filterField])) {
                    response.push(db[i]);
                }
            }
        } else {
            response = [].concat(db);
        }

        if (orderField) {
            response.sort(sortCompare.bind(null, orderField, order));
        }

        return {
            count:  response.length,
            offset: form.get('offset'),
            limit:  form.get('limit'),
            data:   response.slice(offset, offset + limit)
        };
    }
};

/**
 * Helper method for sorting
 *
 * @private
 * @param {string} fieldName
 * @param {string} order
 * @param {User} a
 * @param {User} b
 * @returns {number}
 */
function sortCompare(fieldName, order, a, b) {
    let x = 0;

    if (a[fieldName] < b[fieldName]) {
        x = -1;
    } else if (a[fieldName] > b[fieldName]) {
        x = 1;
    }

    if (0 !== x && 'desc' === order) {
        x *= -1;
    }

    return x;
}

/**
 * Helper method
 *
 * Returns a filter regexp
 *
 * @private
 * @param {string} filterText
 * @returns {RegExp}
 */
function createFilterRegexp(filterText) {
    return new RegExp('^' + filterText.replace(/%/g, '.*').replace(/_/g, '.') + '$', 'i');
}
