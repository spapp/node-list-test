module.exports = {
    Application:     require('./Application'),
    FrontController: require('./FrontController'),
    Router:          require('./Router')
};
